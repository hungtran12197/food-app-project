package com.foodie.app.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Image implements Parcelable {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("category")
    @Expose
    private int category;
    @SerializedName("subcategory")
    @Expose
    private int subcategory;
    @SerializedName("disk_driver")
    @Expose
    private String diskDriver;
    @SerializedName("disk_root")
    @Expose
    private String diskRoot;
    @SerializedName("title")
    @Expose
    private Object title;
    @SerializedName("content")
    @Expose
    private Object content;
    @SerializedName("file_name")
    @Expose
    private String fileName;
    @SerializedName("file_size")
    @Expose
    private int fileSize;
    @SerializedName("file_width")
    @Expose
    private Object fileWidth;
    @SerializedName("file_height")
    @Expose
    private Object fileHeight;
    @SerializedName("upload_file")
    @Expose
    private Object uploadFile;
    @SerializedName("original_name")
    @Expose
    private String originalName;
    @SerializedName("mime_type")
    @Expose
    private String mimeType;
    @SerializedName("file_url")
    @Expose
    private String fileUrl;
    @SerializedName("file_square_url")
    @Expose
    private Object fileSquareUrl;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Image(int id, int category, int subcategory, String diskDriver, String diskRoot, Object title, Object content, String fileName, int fileSize, Object fileWidth, Object fileHeight, Object uploadFile, String originalName, String mimeType, String fileUrl, Object fileSquareUrl, String createdAt, String updatedAt) {
        this.id = id;
        this.category = category;
        this.subcategory = subcategory;
        this.diskDriver = diskDriver;
        this.diskRoot = diskRoot;
        this.title = title;
        this.content = content;
        this.fileName = fileName;
        this.fileSize = fileSize;
        this.fileWidth = fileWidth;
        this.fileHeight = fileHeight;
        this.uploadFile = uploadFile;
        this.originalName = originalName;
        this.mimeType = mimeType;
        this.fileUrl = fileUrl;
        this.fileSquareUrl = fileSquareUrl;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public int getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(int subcategory) {
        this.subcategory = subcategory;
    }

    public String getDiskDriver() {
        return diskDriver;
    }

    public void setDiskDriver(String diskDriver) {
        this.diskDriver = diskDriver;
    }

    public String getDiskRoot() {
        return diskRoot;
    }

    public void setDiskRoot(String diskRoot) {
        this.diskRoot = diskRoot;
    }

    public Object getTitle() {
        return title;
    }

    public void setTitle(Object title) {
        this.title = title;
    }

    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getFileSize() {
        return fileSize;
    }

    public void setFileSize(int fileSize) {
        this.fileSize = fileSize;
    }

    public Object getFileWidth() {
        return fileWidth;
    }

    public void setFileWidth(Object fileWidth) {
        this.fileWidth = fileWidth;
    }

    public Object getFileHeight() {
        return fileHeight;
    }

    public void setFileHeight(Object fileHeight) {
        this.fileHeight = fileHeight;
    }

    public Object getUploadFile() {
        return uploadFile;
    }

    public void setUploadFile(Object uploadFile) {
        this.uploadFile = uploadFile;
    }

    public String getOriginalName() {
        return originalName;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public Object getFileSquareUrl() {
        return fileSquareUrl;
    }

    public void setFileSquareUrl(Object fileSquareUrl) {
        this.fileSquareUrl = fileSquareUrl;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    protected Image(Parcel in) {
        id = in.readInt();
        category = in.readInt();
        subcategory = in.readInt();
        diskDriver = in.readString();
        diskRoot = in.readString();
        title = (Object) in.readValue(Object.class.getClassLoader());
        content = (Object) in.readValue(Object.class.getClassLoader());
        fileName = in.readString();
        fileSize = in.readInt();
        fileWidth = (Object) in.readValue(Object.class.getClassLoader());
        fileHeight = (Object) in.readValue(Object.class.getClassLoader());
        uploadFile = (Object) in.readValue(Object.class.getClassLoader());
        originalName = in.readString();
        mimeType = in.readString();
        fileUrl = in.readString();
        fileSquareUrl = (Object) in.readValue(Object.class.getClassLoader());
        createdAt = in.readString();
        updatedAt = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(category);
        dest.writeInt(subcategory);
        dest.writeString(diskDriver);
        dest.writeString(diskRoot);
        dest.writeValue(title);
        dest.writeValue(content);
        dest.writeString(fileName);
        dest.writeInt(fileSize);
        dest.writeValue(fileWidth);
        dest.writeValue(fileHeight);
        dest.writeValue(uploadFile);
        dest.writeString(originalName);
        dest.writeString(mimeType);
        dest.writeString(fileUrl);
        dest.writeValue(fileSquareUrl);
        dest.writeString(createdAt);
        dest.writeString(updatedAt);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Image> CREATOR = new Parcelable.Creator<Image>() {
        @Override
        public Image createFromParcel(Parcel in) {
            return new Image(in);
        }

        @Override
        public Image[] newArray(int size) {
            return new Image[size];
        }
    };
}
