package com.foodie.app.model;

import com.google.gson.annotations.SerializedName;

public class Slide {
    @SerializedName("file_url")
    private String fileUrl;

    public Slide(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }
}
