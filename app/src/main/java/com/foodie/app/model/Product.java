package com.foodie.app.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Product implements Parcelable {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("product_category_id")
    @Expose
    private int productCategoryId;
    @SerializedName("product_sub_category_id")
    @Expose
    private Object productSubCategoryId;
    @SerializedName("image")
    @Expose
    private Image image;
    @SerializedName("image_detail")
    @Expose
    private Object imageDetail;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("price")
    @Expose
    private int price;
    @SerializedName("discount")
    @Expose
    private int discount;
    @SerializedName("status")
    @Expose
    private Object status;
    @SerializedName("price_text")
    @Expose
    private String priceText;
    @SerializedName("discount_text")
    @Expose
    private String discountText;
    @SerializedName("onsale")
    @Expose
    private int onsale;
    @SerializedName("onsale_text")
    @Expose
    private String onsaleText;
    private int quantity;
    private String note;

    public Product(int id, int productCategoryId, Object productSubCategoryId, Image image, Object imageDetail, String title, String description, int price, int discount, Object status, String priceText, String discountText, int onsale, String onsaleText, int quantity, String note) {
        this.id = id;
        this.productCategoryId = productCategoryId;
        this.productSubCategoryId = productSubCategoryId;
        this.image = image;
        this.imageDetail = imageDetail;
        this.title = title;
        this.description = description;
        this.price = price;
        this.discount = discount;
        this.status = status;
        this.priceText = priceText;
        this.discountText = discountText;
        this.onsale = onsale;
        this.onsaleText = onsaleText;
        this.quantity = quantity;
        this.note = note;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProductCategoryId() {
        return productCategoryId;
    }

    public void setProductCategoryId(int productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

    public Object getProductSubCategoryId() {
        return productSubCategoryId;
    }

    public void setProductSubCategoryId(Object productSubCategoryId) {
        this.productSubCategoryId = productSubCategoryId;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public Object getImageDetail() {
        return imageDetail;
    }

    public void setImageDetail(Object imageDetail) {
        this.imageDetail = imageDetail;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }

    public String getPriceText() {
        return priceText;
    }

    public void setPriceText(String priceText) {
        this.priceText = priceText;
    }

    public String getDiscountText() {
        return discountText;
    }

    public void setDiscountText(String discountText) {
        this.discountText = discountText;
    }

    public int getOnsale() {
        return onsale;
    }

    public void setOnsale(int onsale) {
        this.onsale = onsale;
    }

    public String getOnsaleText() {
        return onsaleText;
    }

    public void setOnsaleText(String onsaleText) {
        this.onsaleText = onsaleText;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    protected Product(Parcel in) {
        id = in.readInt();
        productCategoryId = in.readInt();
        productSubCategoryId = (Object) in.readValue(Object.class.getClassLoader());
        image = (Image) in.readValue(Image.class.getClassLoader());
        imageDetail = (Object) in.readValue(Object.class.getClassLoader());
        title = in.readString();
        description = in.readString();
        price = in.readInt();
        discount = in.readInt();
        status = (Object) in.readValue(Object.class.getClassLoader());
        priceText = in.readString();
        discountText = in.readString();
        onsale = in.readInt();
        onsaleText = in.readString();
        quantity = in.readInt();
        note = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(productCategoryId);
        dest.writeValue(productSubCategoryId);
        dest.writeValue(image);
        dest.writeValue(imageDetail);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeInt(price);
        dest.writeInt(discount);
        dest.writeValue(status);
        dest.writeString(priceText);
        dest.writeString(discountText);
        dest.writeInt(onsale);
        dest.writeString(onsaleText);
        dest.writeInt(quantity);
        dest.writeString(note);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Product> CREATOR = new Parcelable.Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };
}