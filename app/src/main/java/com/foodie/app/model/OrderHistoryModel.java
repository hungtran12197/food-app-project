package com.foodie.app.model;

import java.util.List;

public class OrderHistoryModel {
    private String date;
    private int totalPrice;
    private String lastName;
    private String firstName;
    private String phoneNumber;
    private String emailAddress;
    private String deliveryAddress;
    private Boolean isVisiable;
    private List<Product> item;

    public OrderHistoryModel(){

    }

    public OrderHistoryModel(String date, int totalPrice, String lastName, String firstName, String phoneNumber, String emailAddress, String deliveryAddress,Boolean isVisiable) {
        this.date = date;
        this.totalPrice = totalPrice;
        this.lastName = lastName;
        this.firstName = firstName;
        this.phoneNumber = phoneNumber;
        this.emailAddress = emailAddress;
        this.deliveryAddress = deliveryAddress;
        this.isVisiable = isVisiable;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public Boolean getVisiable() {
        return isVisiable;
    }

    public void setVisiable(Boolean visiable) {
        isVisiable = visiable;
    }

    public List<Product> getItem() {
        return item;
    }

    public void setItem(List<Product> item) {
        this.item = item;
    }
}
