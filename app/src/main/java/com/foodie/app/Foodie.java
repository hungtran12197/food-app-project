package com.foodie.app;

import android.app.Application;

import com.google.gson.Gson;

public class Foodie extends Application {
    private static Foodie mSelf;
    private Gson mGSon;

    public static Foodie self() {
        return mSelf;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mSelf = this;
        mGSon = new Gson();
    }

    public Gson getGSon() {
        return mGSon;
    }
}
