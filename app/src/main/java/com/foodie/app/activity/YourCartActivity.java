package com.foodie.app.activity;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeErrorDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.interfaces.Closure;
import com.foodie.app.R;
import com.foodie.app.adapter.YourCartAdapter;
import com.foodie.app.model.Product;
import com.foodie.app.util.SharedPrefs;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class YourCartActivity extends AppCompatActivity implements YourCartAdapter.OnRemoveClickListener, YourCartAdapter.OnStepperTouchCallbackListener{
    public static final String TOTAL_COUNT = "total_count";
    public static final String CART_DATA = "cart_data";
    public int subFragments = 0;

    ActionBar toolbar;
    Resources res;
    List<Product> savedList;
    float price = 0;
    TextView total_price;
    EditText note;
    Button buttonPay;
    Gson gson;
    RecyclerView cartRecyclerView;
    YourCartAdapter yourCartAdapter;
    LinearLayoutManager layoutManager;
    LinearLayout bottom_btn;
    ImageView closeBtn;
    ImageView backBtn;
    TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_your_cart);
        res = getResources();
        toolbar = getSupportActionBar();
        gson = new Gson();
        bottom_btn = findViewById(R.id.bottom_btn);
        if (toolbar != null) {
            toolbar.setDisplayShowCustomEnabled(true);
            toolbar.setCustomView(R.layout.activity_your_cart_action_bar_layout);
            closeBtn = findViewById(R.id.activity_your_cart_action_bar_close);
            backBtn = findViewById(R.id.back_cart_btn);
            title = findViewById(R.id.activity_your_cart_action_bar_title);
            backBtn.setVisibility(View.GONE);
            closeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }
        total_price = findViewById(R.id.total_price);
        note = findViewById(R.id.note);
        buttonPay = findViewById(R.id.button_order_pay);
        String json =  SharedPrefs.getInstance().get(CART_DATA, String.class);
        Type typeCartData = new TypeToken<List<Product>>(){}.getType();
        savedList = gson.fromJson(json, typeCartData);
        layoutManager = new LinearLayoutManager(this);
        if (savedList != null && savedList.size() > 0) {
            for (int i = 0; i < savedList.size(); i++) {
                Product product = savedList.get(i);
                if (product.getOnsale() > 0) {
                    setTotalPrice(getQuantityPrice(product.getOnsale(), product.getQuantity()), true);
                } else {
                    setTotalPrice(getQuantityPrice(product.getPrice(), product.getQuantity()), true);
                }
            }
            String price_text = "$"+ String.format("%.2f", price);
            total_price.setText(price_text);
            note.setVisibility(View.VISIBLE);
            cartRecyclerView = findViewById(R.id.your_cart_recycler_view);
            cartRecyclerView.setLayoutManager(layoutManager);
            yourCartAdapter = new YourCartAdapter(this, savedList);
            yourCartAdapter.setOnRemoveClickListener(this);
            yourCartAdapter.setOnStepperTouchCallbackListener(this);
            cartRecyclerView.setAdapter(yourCartAdapter);
        } else {
            String price_text = "$"+ String.format("%.2f", price);
            total_price.setText(price_text);
        }

        buttonPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (savedList != null) {
                    if(savedList.size() > 0){
                        Intent myIntent = new Intent(getBaseContext(), CheckoutActivity.class);
                        startActivity(myIntent);
//                        SharedPrefs.getInstance().clear();
//                        savedList.clear();
//                        yourCartAdapter = new YourCartAdapter(YourCartActivity.this, savedList);
//                        cartRecyclerView.setAdapter(yourCartAdapter);
//                        note.setVisibility(View.GONE);
//                        total_price.setText("$0.00");
//                        createSuccessDialog(res.getString(R.string.order_done));
                    } else {
                        createWarningDialog(res.getString(R.string.NO_ITEM_IN_CART));
                    }
                } else {
                    createWarningDialog(res.getString(R.string.NO_ITEM_IN_CART));
                }
            }
        });
    }

    @Override
    public void onStepperTouchCallback(int position, int value, boolean positive) {
        Product product= savedList.get(position);
        product.setQuantity(value);

        int total_count = SharedPrefs.getInstance().get(TOTAL_COUNT, Integer.class);
        if (positive) {
            total_count += 1;
            if (product.getOnsale() > 0) {
                setTotalPrice(product.getOnsale(), true);
            } else {
                setTotalPrice(product.getPrice(), true);
            }
        } else {
            total_count -= 1;
            if (product.getOnsale() > 0) {
                setTotalPrice(product.getOnsale(), false);
            } else {
                setTotalPrice(product.getPrice(), false);
            }
        }

        SharedPrefs.getInstance().put(TOTAL_COUNT, total_count);
        SharedPrefs.getInstance().put(CART_DATA, savedList);

        String price_text = "$"+ String.format("%.2f", price);
        total_price.setText(price_text);
    }

    @Override
    public void onRemoveClick(int position) {
        Product product= savedList.get(position);
        int curQuantity = product.getQuantity();
        float curPrice = 0;
        if (product.getOnsale() > 0) {
            curPrice = product.getOnsale();
        } else {
            curPrice = product.getPrice();
        }

        float priceWithQuantity = curPrice * curQuantity;
        setTotalPrice(priceWithQuantity, false);
        savedList.remove(position);
        yourCartAdapter = new YourCartAdapter(this, savedList);
        yourCartAdapter.setOnRemoveClickListener(this);
        yourCartAdapter.setOnStepperTouchCallbackListener(this);
        cartRecyclerView.setLayoutManager(layoutManager);
        cartRecyclerView.setAdapter(yourCartAdapter);
        int total_count = SharedPrefs.getInstance().get(TOTAL_COUNT, Integer.class);
        total_count -= curQuantity;
        SharedPrefs.getInstance().put(TOTAL_COUNT, total_count);
        SharedPrefs.getInstance().put(CART_DATA, savedList);
        String price_text = "$"+ String.format("%.2f", price);
        total_price.setText(price_text);
        if (savedList.size() == 0) {
            note.setVisibility(View.GONE);
        }
    }

    public void setTotalPrice (float value, boolean positive) {
        if (positive) price +=value;
        else price -= value;
    }

    public float getQuantityPrice (float value, int quantity) {
        float result = value * quantity;
        return result;
    }


    public void createWarningDialog(String message) {
        AwesomeErrorDialog dialog = new AwesomeErrorDialog(this);
        dialog.setTitle(res.getString(R.string.notice));
        dialog.setMessage(message);
        dialog.setColoredCircle(R.color.dialogWarningBackgroundColor);
        dialog.setDialogIconAndColor(R.drawable.ic_dialog_warning, R.color.white);
        dialog.setCancelable(true);
        dialog.setButtonText(getString(R.string.dialog_ok_button));
        dialog.setButtonBackgroundColor(R.color.dialogWarningBackgroundColor);
        dialog.setButtonText(getString(R.string.dialog_ok_button));
        dialog.setErrorButtonClick(new Closure() {
            @Override
            public void exec() {
                // TODO
            }
        });
        dialog.show();
    }

    public void createSuccessDialog(String message) {
        AwesomeErrorDialog dialog = new AwesomeErrorDialog(this);
        dialog.setTitle(res.getString(R.string.notice));
        dialog.setMessage(message);
        dialog.setColoredCircle(R.color.dialogSuccessBackgroundColor);
        dialog.setDialogIconAndColor(R.drawable.ic_success, R.color.white);
        dialog.setCancelable(true);
        dialog.setButtonText(getString(R.string.dialog_ok_button));
        dialog.setButtonBackgroundColor(R.color.dialogSuccessBackgroundColor);
        dialog.setButtonText(getString(R.string.dialog_ok_button));
        dialog.setErrorButtonClick(new Closure() {
            @Override
            public void exec() {
                finish();
            }
        });
        dialog.show();
    }

}
