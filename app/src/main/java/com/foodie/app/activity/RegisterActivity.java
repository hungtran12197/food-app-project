package com.foodie.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.foodie.app.R;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {
    Button back;
    EditText username, password, repassword,email,phonenumber;
    Button btnRegister, btnBackLogin;
    DBHelper myDB;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
        back = findViewById(R.id.btnBackLogin);
        back.setOnClickListener(this);
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        repassword = (EditText) findViewById(R.id.repassword);
        email = (EditText)findViewById(R.id.email);
        phonenumber = (EditText)findViewById(R.id.phonenumber);

        btnRegister = (Button) findViewById(R.id.btnRegister);
        btnBackLogin = (Button) findViewById(R.id.btnBackLogin);
        myDB = new DBHelper(this);
        onClickAgree();

    }


    private void onClickAgree() {
        btnRegister.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                String user = username.getText().toString();
                String pass = password.getText().toString();
                String repass = repassword.getText().toString();
                String emailReg = email.getText().toString();
                String phoneReg = phonenumber.getText().toString();
                if (user.equals("") || password.equals("") || repassword.equals("")) {
                    Toast.makeText(RegisterActivity.this, "Fill all the fields", Toast.LENGTH_SHORT).show();
                } else {
                    if (pass.equals(repass)) {
                        Boolean usercheckResult = myDB.checkusername(user);
                        if (usercheckResult == false) {
                            Boolean regResult = myDB.insertData(user, pass,emailReg,phoneReg);
                            if (regResult == true) {
                                Toast.makeText(RegisterActivity.this, "Registation successful", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(RegisterActivity.this, "Registation Failed", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(RegisterActivity.this, "User already Exists.", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(RegisterActivity.this, "Password not matching", Toast.LENGTH_SHORT).show();
                    }

                }

            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnBackLogin:
                finish();
                break;

        }
    }
}
