package com.foodie.app.activity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.content.res.Resources;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeErrorDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.interfaces.Closure;
import com.foodie.app.R;
import com.foodie.app.fragments.ItemCheckout;
import com.foodie.app.model.OrderHistoryModel;
import com.foodie.app.model.Product;
import com.foodie.app.util.CommonKey;
import com.foodie.app.util.SharedPrefs;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class CheckoutActivity extends AppCompatActivity implements View.OnClickListener {
    private final Boolean ENABLE_STRIPE = true;
    private final Boolean ENABLE_PAYPAL = false;
    private final Integer PAY_CREDIT_CARD = 0;
    private final Integer PAY_PAYPAL = 1;
    private final Integer PAY_ON_DELIVERY = 2;

    private int payMethod = 0;
    private int total = 0;

    private int expMonth;
    private int expYear;

    FourDigitFormat format;
    List<Product> savedList;
    List<OrderHistoryModel> orderHistory;
    ActionBar toolBar;
    Gson gson;
    Resources res;
    ImageView closeBtn;
    ImageView backBtn;
    TextView title;
    LinearLayout itemLayout;
    TextView totalCheckout;
    Button payCreditCard;
    Button paypal;
    Button payOnDelivery;
    LinearLayout cardDetailLayout;
    EditText cardNr;
    ImageView calender;
    TextView expDate;
    EditText cvc;
    EditText holderName;
    Button dismiss;
    Button placeOrder;
    EditText firstName;
    EditText lastName;
    EditText phoneNumber;
    EditText email;
    EditText address;
    CheckBox saveDataUser;
    CheckBox saveDataPayment;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        res = getResources();
        toolBar = getSupportActionBar();
        totalCheckout = findViewById(R.id.total_checkout_price);
        gson = new Gson();

        format = new FourDigitFormat();

        RelativeLayout payment_buttons_layout = findViewById(R.id.payment_buttons_layout);
        View view = LayoutInflater.from(getBaseContext()).inflate(getPaymentButtonsResId(), payment_buttons_layout, true);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.BELOW, R.id.Payment);
        view.setLayoutParams(params);

        payCreditCard = findViewById(R.id.payCreditCard);
        paypal = findViewById(R.id.payPayPal);
        payOnDelivery = findViewById(R.id.payDelivery);
        cardDetailLayout = findViewById(R.id.cardDetailsLayout);
        cardNr = findViewById(R.id.cardNr);
        calender = findViewById(R.id.displayCalendar);
        expDate = findViewById(R.id.expDate);
        cvc = findViewById(R.id.CVC);
        holderName = findViewById(R.id.holderName);
        dismiss = findViewById(R.id.dismissOrderButton);
        placeOrder = findViewById(R.id.placeOrderButton);
        firstName = findViewById(R.id.firstName);
        lastName = findViewById(R.id.COLastName);
        phoneNumber = findViewById(R.id.COphone);
        email = findViewById(R.id.COemail);
        address = findViewById(R.id.COAdress);
        saveDataUser = findViewById(R.id.saveDataCheckbox);
        saveDataPayment = findViewById(R.id.saveDataCardCheckbox);

        cardNr.addTextChangedListener(format);
        if(payCreditCard != null){
            payCreditCard.setOnClickListener(this);
        }
        if(paypal != null){
            paypal.setOnClickListener(this);
        }
        payOnDelivery.setOnClickListener(this);
        calender.setOnClickListener(this);
        dismiss.setOnClickListener(this);
        placeOrder.setOnClickListener(this);

        if (toolBar != null) {
            toolBar.setDisplayShowCustomEnabled(true);
            toolBar.setCustomView(R.layout.activity_your_cart_action_bar_layout);
            closeBtn = findViewById(R.id.activity_your_cart_action_bar_close);
            backBtn = findViewById(R.id.back_cart_btn);
            title = findViewById(R.id.activity_your_cart_action_bar_title);
            closeBtn.setVisibility(View.GONE);
            title.setText("Check out");
            backBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }
        String json =  SharedPrefs.getInstance().get(CommonKey.CART_DATA, String.class);
        Type typeOfList = new TypeToken<List<Product>>(){}.getType();
        savedList = gson.fromJson(json, typeOfList);

        itemLayout = findViewById(R.id.item);
        itemLayout.removeAllViews();
        for (Product item : savedList){
            itemLayout.addView(new ItemCheckout(getBaseContext(), null, item));
            total += item.getOnsale() * item.getQuantity();
        }
        totalCheckout.setText("$"+total);

        initData();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.payCreditCard:
                paywithCreditCard();
                cardDetailLayout.setVisibility(View.VISIBLE);
                break;
            case R.id.payPayPal:
                payWithPaypal();
                cardDetailLayout.setVisibility(View.GONE);
                break;
            case R.id.payDelivery:
                payOnDelivery();
                cardDetailLayout.setVisibility(View.GONE);
                break;
            case R.id.displayCalendar:
                int year = Calendar.getInstance().get(Calendar.YEAR);
                int month = Calendar.getInstance().get(Calendar.MONTH);
                int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePicker = new DatePickerDialog(this, android.R.style.Theme_Material_Light_Dialog, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
                        expDate.setText(cardExpiryDate(year, month));
                        expDate.setError(null);
                    }
                }, year, month, day);
                datePicker.getDatePicker().setMinDate(System.currentTimeMillis());
                datePicker.show();
                break;
            case R.id.placeOrderButton:
                if(payMethod == PAY_CREDIT_CARD){
                    validateCardInfor();
                    validatePersonInfor();
                    if(checkPersonInfor() && checkCardInfor()){
                        createSuccessDialog(res.getString(R.string.order_done));
                        saveData(saveDataUser.isChecked(), saveDataPayment.isChecked());
                    }
                } else {
                    validatePersonInfor();
                    if(checkPersonInfor()){
                        createSuccessDialog(res.getString(R.string.order_done));
                        saveData(saveDataUser.isChecked(), saveDataPayment.isChecked());
                    }
                }

                break;
            case R.id.dismissOrderButton:
                break;
        }
    }

    private String formatCardNr(String cardNr) {
        cardNr = cardNr.replace("-", "");
        return cardNr;
    }

    private void initData() {
        SharedPreferences sharedPref = this.getSharedPreferences(CommonKey.INFORMATION_KEY, Context.MODE_PRIVATE);
        if(sharedPref.contains(CommonKey.PERSONAL_INFORMATION)){
            String dataUser = sharedPref.getString(CommonKey.PERSONAL_INFORMATION, "");
            String dataCard = sharedPref.getString(CommonKey.CARD_INFORMATION,"");
            JSONObject objectUser = null;
            JSONObject objectCard = null;
            try {
                objectUser = new JSONObject(dataUser);
                Log.d("asdf", objectUser + "");
                if(objectUser.has("firstName")){
                    firstName.setText(objectUser.getString("firstName"));
                }
                if(objectUser.has("lastName")){
                    lastName.setText(objectUser.getString("lastName"));
                }
                if(objectUser.has("phoneNumber")){
                    phoneNumber.setText(objectUser.getString("phoneNumber"));
                }
                if(objectUser.has("email")){
                    email.setText(objectUser.getString("email"));
                }
                if(objectUser.has("address")){
                    address.setText(objectUser.getString("address"));
                }
            } catch (JSONException e){
                e.printStackTrace();
            }
            try {
                objectCard = new JSONObject(dataCard);
                if(objectCard.has("cardNumber")){
                    cardNr.setText(objectCard.getString("cardNumber"));
                }
                if(objectCard.has("expDate")){
                    expDate.setText(objectCard.getString("expDate"));
                }
                if(objectCard.has("cvc")){
                    cvc.setText(objectCard.getString("cvc"));
                }
                if(objectCard.has("holderName")){
                    holderName.setText(objectCard.getString("holderName"));
                }
            } catch (JSONException e){
                e.printStackTrace();
            }
        }
    }

    private void saveData(Boolean userData, Boolean cardData) {
        JSONObject dataUser = new JSONObject();
        JSONObject dataCard = new JSONObject();
        if(userData || cardData){
            SharedPreferences sharedPref = this.getSharedPreferences(CommonKey.INFORMATION_KEY, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            if(userData){
                try {
                    dataUser.put("firstName", firstName.getText().toString());
                    dataUser.put("lastName", lastName.getText().toString());
                    dataUser.put("phoneNumber", phoneNumber.getText().toString());
                    dataUser.put("email", email.getText().toString());
                    dataUser.put("address", address.getText().toString());
                    editor.putString(CommonKey.PERSONAL_INFORMATION, dataUser.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if(cardData){
                try {
                    dataCard.put("cardNumber", cardNr.getText().toString());
                    dataCard.put("expDate", expDate.getText().toString());
                    dataCard.put("cvc", cvc.getText().toString());
                    dataCard.put("holderName", holderName.getText().toString());
                    editor.putString(CommonKey.CARD_INFORMATION, dataCard.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            editor.commit();
        }
    }

    private void validateCardInfor(){
        if(cardNr.getText().toString().isEmpty() || cardNr.getText().toString().length() < 19){
            cardNr.setError("Card number must have 16 characters!");
        } else {
            cardNr.setError(null);
        }
        if (expDate.getText().toString().equals("Exp date")){
            expDate.setError("");
        }  else {
            expDate.setError(null);
        }
        if (cvc.getText().toString().isEmpty() || cvc.getText().toString().length() < 3){
            cvc.setError("CVC have to 3 or 4 characters");
        } else {
            cvc.setError(null);
        }
        if (holderName.getText().toString().isEmpty()){
            holderName.setError("This filed cannot be empty");
        } else {
            holderName.setError(null);
        }
    }

    private void validatePersonInfor(){
        if(phoneNumber.getText().toString().isEmpty()){
            phoneNumber.setError("This filed cannot be empty");
        } else {
            phoneNumber.setError(null);
        }
        if (lastName.getText().toString().isEmpty()){
            lastName.setError("This filed cannot be empty");
        }  else {
            lastName.setError(null);
        }
        if (firstName.getText().toString().isEmpty()){
            firstName.setError("This filed cannot be empty");
        } else {
            firstName.setError(null);
        }
        if (email.getText().toString().isEmpty()){
            email.setError("This filed cannot be empty");
        } else {
            email.setError(null);
        }
        if (address.getText().toString().isEmpty()){
            address.setError("This filed cannot be empty");
        } else {
            address.setError(null);
        }
    }

    private Boolean checkPersonInfor(){
        return !lastName.getText().toString().isEmpty()
                && !firstName.getText().toString().isEmpty() && !phoneNumber.getText().toString().isEmpty()
                && !email.getText().toString().isEmpty() && !address.getText().toString().isEmpty();
    }

    private Boolean checkCardInfor(){
        return !cardNr.getText().toString().isEmpty() && cardNr.getText().toString().length() == 19
                && !expDate.getText().toString().equals("Exp date") && !cvc.getText().toString().isEmpty()
                && cvc.getText().toString().length() >= 3 && !holderName.getText().toString().isEmpty();
    }

    private String cardExpiryDate(int year, int month) {
        expYear = year;
        String expiryDate = "";
        month = month + 1;
        expMonth = month;
        String monthString = String.valueOf(month);
        if (month < 10) {
            monthString = "0" + monthString;
        }
        expiryDate = monthString + "-" + year;
        return expiryDate;
    }

    private int getPaymentButtonsResId() {
        if (ENABLE_STRIPE && !ENABLE_PAYPAL)
            return R.layout.paypal_disable_layout;

        if (ENABLE_PAYPAL && !ENABLE_STRIPE) {
            return R.layout.stripe_disable_layout;
        }

        return R.layout.payment_method;
    }

    public void paywithCreditCard(){
        payCreditCard.setTextColor(ContextCompat.getColor(this, R.color.colorWhite));
        payCreditCard.setBackground(ContextCompat.getDrawable(this, R.drawable.pay_credit_pressed));

        if (paypal != null) {
            paypal.setTextColor(ContextCompat.getColor(this, R.color.colorLightGrey));
            paypal.setBackground(ContextCompat.getDrawable(this, R.drawable.pay_paypal_unpressed));
        }
        payOnDelivery.setTextColor(ContextCompat.getColor(this, R.color.colorLightGrey));
        payOnDelivery.setBackground(ContextCompat.getDrawable(this, R.drawable.pay_delivery_unpressed));
//        cardDetails.setVisibility(View.VISIBLE);
        payMethod = PAY_CREDIT_CARD;
    }

    public void payWithPaypal(){
        paypal.setTextColor(ContextCompat.getColor(this, R.color.colorWhite));
        paypal.setBackground(ContextCompat.getDrawable(this, R.drawable.paypal_pressed));

        if (payCreditCard != null) {
            payCreditCard.setTextColor(ContextCompat.getColor(this, R.color.colorLightGrey));
            payCreditCard.setBackground(ContextCompat.getDrawable(this, R.drawable.pay_with_credit_card_unpressed));
        }
        payOnDelivery.setTextColor(ContextCompat.getColor(this, R.color.colorLightGrey));
        payOnDelivery.setBackground(ContextCompat.getDrawable(this, R.drawable.pay_delivery_unpressed));
        payMethod = PAY_PAYPAL;
    }

    public void payOnDelivery(){
        payOnDelivery.setTextColor(ContextCompat.getColor(this, R.color.colorWhite));
        payOnDelivery.setBackground(ContextCompat.getDrawable(this, R.drawable.pay_on_delivery_pressed));

        if (payCreditCard != null) {
            payCreditCard.setTextColor(ContextCompat.getColor(this, R.color.colorLightGrey));
            payCreditCard.setBackground(ContextCompat.getDrawable(this, R.drawable.pay_with_credit_card_unpressed));
        }
        if (paypal != null) {
            paypal.setTextColor(ContextCompat.getColor(this, R.color.colorLightGrey));
            paypal.setBackground(ContextCompat.getDrawable(this, R.drawable.pay_paypal_unpressed));
        }
        payMethod = PAY_ON_DELIVERY;
    }

    public void createSuccessDialog(String message) {
        saveItem();
        SharedPrefs.getInstance().remove(CommonKey.CART_DATA);
        SharedPrefs.getInstance().remove(CommonKey.TOTAL_COUNT);
        AwesomeErrorDialog dialog = new AwesomeErrorDialog(this);
        dialog.setTitle(res.getString(R.string.notice));
        dialog.setMessage(message);
        dialog.setColoredCircle(R.color.dialogSuccessBackgroundColor);
        dialog.setDialogIconAndColor(R.drawable.ic_success, R.color.white);
        dialog.setCancelable(true);
        dialog.setButtonText(getString(R.string.dialog_ok_button));
        dialog.setButtonBackgroundColor(R.color.dialogSuccessBackgroundColor);
        dialog.setButtonText(getString(R.string.dialog_ok_button));
        dialog.setErrorButtonClick(new Closure() {
            @Override
            public void exec() {
                finishAffinity();
                startActivity(new Intent(getBaseContext(), MainActivity.class));
            }
        });
        dialog.show();
    }

    private void saveItem(){
        String json =  SharedPrefs.getInstance().get(CommonKey.CART_HISTORY, String.class);
        Type typeCartData = new TypeToken<List<OrderHistoryModel>>(){}.getType();
        orderHistory = gson.fromJson(json, typeCartData);

        OrderHistoryModel data = new OrderHistoryModel();

        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String strDate = dateFormat.format(new Date());
        data.setDate(strDate);
        data.setTotalPrice(total);
        data.setLastName(lastName.getText().toString());
        data.setFirstName(firstName.getText().toString());
        data.setPhoneNumber(phoneNumber.getText().toString());
        data.setEmailAddress(email.getText().toString());
        data.setDeliveryAddress(address.getText().toString());
        data.setVisiable(false);
        data.setItem(savedList);
        if(orderHistory != null){
            orderHistory.add(data);
        } else {
            orderHistory = new ArrayList<>();
            orderHistory.add(data);
        }

        SharedPrefs.getInstance().put(CommonKey.CART_HISTORY, orderHistory);
    }

    private class FourDigitFormat implements TextWatcher {

        // Change this to what you want... ' ', '-' etc..
        private static final char space = '-';
        private String cardNrString = "";
        private int start;
        private int before;
        private String _ccNumber = "";

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            this.start = start;
            this.before = before;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
//             Remove spacing char
            if (s.length() > 0 && (s.length() % 5) == 0) {
                final char c = s.charAt(s.length() - 1);
                if (space == c) {
                    s.delete(s.length() - 1, s.length());
                }
            }
            // Insert char where needed.
            if (s.length() > 0 && (s.length() % 5) == 0) {
                char c = s.charAt(s.length() - 1);
                // Only if its a digit where there should be a space we insert a space
                if (Character.isDigit(c) && TextUtils.split(s.toString(), String.valueOf(space)).length <= 3) {
                    s.insert(s.length() - 1, String.valueOf(space));
                }
            }

            formatCardNr(s);
        }

        private void formatCardNr(Editable string) {
            String string1 = String.valueOf(string).replace("-", "");
            for (int i = 0; i < string1.length(); i++) {
                if (i == 4 || i == 9 || i == 14)
                    string1 = new StringBuilder(string1).insert(i, "-").toString();
            }

            cardNr.removeTextChangedListener(format);
            cardNr.setText(string1);
            cardNr.setSelection(before == 1 ? start : cardNr.getText().toString().length());
            cardNr.addTextChangedListener(format);

        }

    }
}
