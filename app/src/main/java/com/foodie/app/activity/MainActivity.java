package com.foodie.app.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.foodie.app.R;
import com.foodie.app.adapter.ProductAdapter;
import com.foodie.app.adapter.RecommendedAdapter;
import com.foodie.app.adapter.SlideAdapter;
import com.foodie.app.fragments.AboutFragment;
import com.foodie.app.fragments.OrderHistory;
import com.foodie.app.model.Content;
import com.foodie.app.model.Product;
import com.foodie.app.model.Slide;
import com.foodie.app.util.CommonFunction;
import com.foodie.app.util.CommonKey;
import com.foodie.app.util.SharedPrefs;
import com.foodie.app.util.SpacesItemDecoration;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    public int subFragments = 1;

    ActionBar toolbar;
    Resources res;
    ViewPager viewPager;
    SlideAdapter slideAdapter;
    List<Slide> listSlide;
    List<Content> listContent;
    List<Product> listProduct;
    Timer timer;
    int current_position = 0;
    LinearLayout dotsLayout;
    RecyclerView productRecyclerView, recommendedRecyclerView;
    ProductAdapter productAdapter;
    RecommendedAdapter recommendedAdapter;
    TextView badge;
    DrawerLayout drawer;
    TextView history;
  //  TextView setting;
    TextView about;
    FrameLayout cart;
    ImageView menu;
    ImageView backBtn;
    ImageView deleteBtn;
    TextView titleMenu;
    TextView login;
    EditText username,password,repassword;
    Button btnRegister,btnBackLogin;
    DBHelper myDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        res = getResources();
        drawer = findViewById(R.id.main_drawer_layout);
        history = findViewById(R.id.history_order);
      //  setting = findViewById(R.id.setting);
        about = findViewById(R.id.about);
        about.setOnClickListener(this);
        history.setOnClickListener(this);
      //  setting.setOnClickListener(this);
        toolbar = getSupportActionBar();
        login = findViewById(R.id.loginTxt);
        login.setOnClickListener(this);
        username = (EditText)findViewById(R.id.username);
        password = (EditText)findViewById(R.id.password);
        repassword = (EditText)findViewById(R.id.repassword);

        btnRegister = (Button) findViewById(R.id.btnRegister);
        btnBackLogin = (Button) findViewById(R.id.btnBackLogin);
        myDB = new DBHelper(this);


        if (toolbar != null) {
            toolbar.setDisplayShowCustomEnabled(true);
            toolbar.setCustomView(R.layout.activity_main_action_bar_layout);
            badge = ((TextView)findViewById(R.id.activity_main_action_bar_badge));
            cart = (FrameLayout) findViewById(R.id.activity_main_action_bar_cart);
            menu = findViewById(R.id.activity_main_action_bar_menu);
            backBtn = findViewById(R.id.back_btn);
            titleMenu = findViewById(R.id.activity_main_action_bar_title);
            deleteBtn = findViewById(R.id.delete_history);
            cart.setOnClickListener(this);
            menu.setOnClickListener(this);
            backBtn.setOnClickListener(this);
        }
        checkCartItem();
        Type listSlideType = new TypeToken<List<Slide>>(){}.getType();
        listSlide = new Gson().fromJson(CommonFunction.loadJSONFromAsset(this,"header_photos.json"), listSlideType);
        viewPager = findViewById(R.id.slider);
        slideAdapter = new SlideAdapter(this, listSlide);
        viewPager.setAdapter(slideAdapter);
        dotsLayout = findViewById(R.id.dotsContainer);
        prepareDots(0);
        createSlideShow();
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                prepareDots(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        LinearLayoutManager contentLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
        Type listContentType = new TypeToken<List<Content>>(){}.getType();
        listContent = new Gson().fromJson(CommonFunction.loadJSONFromAsset(this,"product_category.json"), listContentType);
        productRecyclerView = findViewById(R.id.product_recycler_view);
        productRecyclerView.setLayoutManager(contentLayoutManager);
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.dimens_8dp);
        productRecyclerView.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
        productRecyclerView.setItemAnimator(new DefaultItemAnimator());
        productAdapter = new ProductAdapter(this, listContent);
        productRecyclerView.setAdapter(productAdapter);

        LinearLayoutManager recommendedLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        Type listProductType = new TypeToken<List<Product>>(){}.getType();
        listProduct = new Gson().fromJson(CommonFunction.loadJSONFromAsset(this,"recommend_product.json"), listProductType);
        recommendedRecyclerView = findViewById(R.id.recommended_recycler_view);
        recommendedRecyclerView.setLayoutManager(recommendedLayoutManager);
        recommendedAdapter = new RecommendedAdapter(this, listProduct);
        recommendedRecyclerView.setAdapter(recommendedAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        checkCartItem();
    }

    private void addFragment(int fragmentReplace, Fragment newFragment ){
        FragmentManager ft = getSupportFragmentManager();
        ft.beginTransaction().replace(fragmentReplace, newFragment).addToBackStack(null).commit();
        subFragments++;
    }

    private void popFragment(){
        while(subFragments > 0){
            getSupportFragmentManager().popBackStackImmediate();
            subFragments--;
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.loginTxt:
                Intent loginIntent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(loginIntent);
                closeDrawer();
                break;

            case R.id.activity_main_action_bar_cart:
                Intent yourCartIntent = new Intent(MainActivity.this, YourCartActivity.class);
                startActivity(yourCartIntent);
                closeDrawer();
                break;
            case R.id.activity_main_action_bar_menu:
                if(drawer.isDrawerOpen(Gravity.LEFT)){
                    closeDrawer();
                } else {
                    drawer.openDrawer(Gravity.LEFT);
                }
                break;
            case R.id.history_order:
                addFragment(R.id.main_fragment, new OrderHistory());
                menu.setVisibility(View.GONE);
                backBtn.setVisibility(View.VISIBLE);
                cart.setVisibility(View.GONE);
                deleteBtn.setVisibility(View.VISIBLE);
                titleMenu.setText("Order History");
                closeDrawer();
                break;
         //   case R.id.setting:
            //    Log.d("asdf","setting");
            //    closeDrawer();
               // break;
            case R.id.back_btn:
                popFragment();
                menu.setVisibility(View.VISIBLE);
                backBtn.setVisibility(View.GONE);
                cart.setVisibility(View.VISIBLE);
                deleteBtn.setVisibility(View.GONE);
                titleMenu.setText("Foodie");
                break;
            case R.id.about:
                addFragment(R.id.main_fragment, new AboutFragment());
                menu.setVisibility(View.GONE);
                backBtn.setVisibility(View.VISIBLE);
                cart.setVisibility(View.GONE);
                deleteBtn.setVisibility(View.GONE);
                titleMenu.setText("About App");
                closeDrawer();
                break;

        }
    }

    private void closeDrawer(){
        if(drawer.isDrawerOpen(Gravity.LEFT)){
            drawer.closeDrawer(Gravity.LEFT);
        }
    }

    private void createSlideShow() {
        final Handler handler = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (current_position == listSlide.size()) {
                    current_position = 0;
                }
                viewPager.setCurrentItem(current_position++, true);
            }
        };
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(runnable);
            }
        }, 250, 2500);
    }

    private void prepareDots(int currentSlidePosition) {
        if (dotsLayout.getChildCount() > 0) {
            dotsLayout.removeAllViews();
        }
        ImageView dots[] = new ImageView[listSlide.size()];
        for (int i = 0; i < listSlide.size(); i ++) {
            dots[i] = new ImageView(this);
            if (i == currentSlidePosition) {
                dots[i].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.active_dot));
            } else {
                dots[i].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.inactive_dot));
            }
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(15, 0, 15, 0);
            dotsLayout.addView(dots[i], layoutParams);
        }
    }

    private void checkCartItem() {
        int cartBadge = SharedPrefs.getInstance().get(CommonKey.TOTAL_COUNT, Integer.class);
        if (cartBadge > 0) {
            badge.setText(String.valueOf(cartBadge));
            badge.setVisibility(View.VISIBLE);
        } else {
            badge.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("requestCode",""+requestCode);
        Log.d("resultCode",""+resultCode);

    }
}
