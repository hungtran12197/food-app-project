package com.foodie.app.activity;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.foodie.app.R;
import com.foodie.app.adapter.OrderAdapter;
import com.foodie.app.model.Product;
import com.foodie.app.util.SharedPrefs;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tbuonomo.creativeviewpager.CreativeViewPager;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class OrderActivity extends AppCompatActivity implements OrderAdapter.OnItemClickListener{
    public static final String TOTAL_COUNT = "total_count";
    public static final String CART_DATA = "cart_data";

    ActionBar toolbar;
    Resources res;
    List<Product> listProduct, savedList;
    CreativeViewPager creativeViewPagerView;
    OrderAdapter orderAdapter;
    TextView badge;
    Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        Intent productIntent = getIntent();
        res = getResources();
        toolbar = getSupportActionBar();
        gson = new Gson();
        if (toolbar != null) {
            toolbar.setDisplayShowCustomEnabled(true);
            toolbar.setCustomView(R.layout.activity_order_action_bar_layout);
            badge = ((TextView)findViewById(R.id.activity_order_action_bar_badge));
            ((TextView)findViewById(R.id.activity_order_action_bar_title)).setText(productIntent.getStringExtra("title"));
            ((ImageView)findViewById(R.id.activity_order_action_bar_back)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
            ((FrameLayout)findViewById(R.id.activity_order_action_bar_cart)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent yourCartIntent = new Intent(OrderActivity.this, YourCartActivity.class);
                    startActivity(yourCartIntent);
                }
            });
        }
        checkCartItem();
        listProduct = productIntent.getParcelableArrayListExtra("products");
        creativeViewPagerView = findViewById(R.id.creativeViewPagerView);
        orderAdapter = new OrderAdapter(this, listProduct);
        orderAdapter.setOnItemClickListener(this);
        creativeViewPagerView.setCreativeViewPagerAdapter(orderAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        checkCartItem();
    }

    @Override
    public void onItemClick(int position) {
        int total_count = 1;
        if (SharedPrefs.getInstance().get(TOTAL_COUNT, Integer.class) == 0) {
            SharedPrefs.getInstance().put(TOTAL_COUNT, total_count);
        } else {
            total_count = SharedPrefs.getInstance().get(TOTAL_COUNT, Integer.class) + 1;
            SharedPrefs.getInstance().put(TOTAL_COUNT, total_count);
        }
        badge.setText(String.valueOf(total_count));
        badge.setVisibility(View.VISIBLE);

        String json =  SharedPrefs.getInstance().get(CART_DATA, String.class);
        Type typeCartData = new TypeToken<List<Product>>(){}.getType();
        savedList = gson.fromJson(json, typeCartData);

        boolean isExist = false;
        if (savedList != null && savedList.size() > 0) {
            for (int i = 0; i < savedList.size(); i++) {
                Log.d("MyLog", "savedList:: "+savedList.get(i).getId());
                Log.d("MyLog", "listProduct:: "+listProduct.get(position).getId());
                if (savedList.get(i).getId() != listProduct.get(position).getId()) {
                    continue;
                }
                int quantity = savedList.get(i).getQuantity() + 1;
                savedList.get(i).setQuantity(quantity);
                isExist = true;
            }
        } else {
            savedList = new ArrayList<>();
        }
        if (!isExist) {
            listProduct.get(position).setQuantity(1);
            listProduct.get(position).setNote("");
            savedList.add(listProduct.get(position));
        }
        SharedPrefs.getInstance().put(CART_DATA, savedList);
    }

    private void checkCartItem() {
        int cartBadge = SharedPrefs.getInstance().get(TOTAL_COUNT, Integer.class);
        if (cartBadge > 0) {
            badge.setText(String.valueOf(cartBadge));
            badge.setVisibility(View.VISIBLE);
        } else {
            badge.setVisibility(View.GONE);
        }
    }
}
