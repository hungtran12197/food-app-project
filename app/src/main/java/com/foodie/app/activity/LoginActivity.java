package com.foodie.app.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.foodie.app.R;

public class    LoginActivity extends AppCompatActivity implements View.OnClickListener {
    Button btnRegister;
    EditText username, password;
    Button btnLogin;
    DBHelper myDB;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        btnRegister = findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(this);
        username = (EditText) findViewById(R.id.username_login);
        password = (EditText) findViewById(R.id.password_login);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        myDB = new DBHelper(this);
        btnLogin.setOnClickListener(this);
    }

    private void login(){
        String user = username.getText().toString().trim();
        String pass = password.getText().toString().trim();

        if (user.equals("") || pass.equals("")) {
            Toast.makeText(LoginActivity.this, "Please full fill your account", Toast.LENGTH_SHORT).show();
        } else {
            Boolean result = myDB.checkusernamePassword(user, pass);

            if (result == true) {
//                Intent LoginIntent = new Intent(LoginActivity.this, MainActivity.class);
//                finish();
                Intent resultIntent = new Intent(this,MainActivity.class);
                resultIntent.putExtra("LOGIN",true );
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
            } else {
                Toast.makeText(LoginActivity.this, "Invalid Username or Password", Toast.LENGTH_SHORT).show();
            }
        }
    }




    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnRegister:
                Intent registerIntent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(registerIntent);
                break;
            case R.id.btnLogin:
                this.login();
                break;

        }
    }
}
