package com.foodie.app.fragments;

import android.app.ActionBar;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.foodie.app.R;
import com.foodie.app.adapter.OrderHistoryAdapter;
import com.foodie.app.model.OrderHistoryModel;
import com.foodie.app.model.Product;
import com.foodie.app.util.CommonKey;
import com.foodie.app.util.SharedPrefs;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class OrderHistory extends Fragment {
    ActionBar toolbar;
    List<OrderHistoryModel> savedList;
    OrderHistoryAdapter orderHistoryAdapter;
    RecyclerView orderCycleView;
    LinearLayoutManager layoutManager;
    Gson gson;
    TextView no_orders;
    ImageView deleteItem;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_order_history, container, false);
        gson = new Gson();
        savedList = new ArrayList<OrderHistoryModel>();
        initData();

        orderCycleView = fragmentView.findViewById(R.id.order_history_recyclerview);
        layoutManager = new LinearLayoutManager(getContext());
        orderCycleView.setLayoutManager(layoutManager);
        orderHistoryAdapter = new OrderHistoryAdapter(getContext(), savedList);
        orderCycleView.setAdapter(orderHistoryAdapter);
        no_orders = fragmentView.findViewById(R.id.no_orders);

        deleteItem  = getActivity().findViewById(R.id.delete_history);
        deleteItem.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                savedList.clear();
                orderHistoryAdapter = new OrderHistoryAdapter(getContext(), savedList);
                orderCycleView.setAdapter(orderHistoryAdapter);
                SharedPrefs.getInstance().remove(CommonKey.CART_HISTORY);
                no_orders.setVisibility(View.VISIBLE);
            }
        });
        if(savedList != null){
            if(savedList.size() > 0){
                no_orders.setVisibility(View.GONE);
            } else {
                no_orders.setVisibility(View.VISIBLE);
            }
        } else {
            no_orders.setVisibility(View.VISIBLE);
        }

        return fragmentView;
    }

    private void initData(){
        String json =  SharedPrefs.getInstance().get(CommonKey.CART_HISTORY, String.class);
        Type typeCartData = new TypeToken<List<OrderHistoryModel>>(){}.getType();
        savedList = gson.fromJson(json, typeCartData);
    }

}
