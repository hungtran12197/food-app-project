package com.foodie.app.fragments;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;

import com.foodie.app.R;
import com.foodie.app.interfaces.UICallback;
import com.foodie.app.model.Product;
import com.foodie.app.util.SharedPrefs;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class ItemCheckout extends RelativeLayout {
    private Context context;
    private UICallback callback;
    private Product product;

    List<Product> savedList;
    Gson gson;
    TextView nameProduct;
    TextView price;
    LinearLayout itemLayout;


    public ItemCheckout(Context context, UICallback callback, Product product){
        super(context);
        this.context = context;
        this.callback = callback;
        this.product = product;

        gson = new Gson();
        String json =  SharedPrefs.getInstance().get("cart_data", String.class);
        Type typeOfList = new TypeToken<List<Product>>(){}.getType();
        savedList = gson.fromJson(json, typeOfList);

        createView();
    }

    private void createView(){
        final RelativeLayout layout = (RelativeLayout) LayoutInflater.from(context).inflate(R.layout.item_checkout, this);
        nameProduct = layout.findViewById(R.id.nameProduct);
        price = layout.findViewById(R.id.price);
        itemLayout = layout.findViewById(R.id.item);
        nameProduct.setText(product.getTitle() + " " + "x" + product.getQuantity());
        int finalPrice = product.getOnsale() * product.getQuantity();
        price.setText("$"+finalPrice);
    }

}
