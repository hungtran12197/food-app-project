package com.foodie.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.foodie.app.R;
import com.foodie.app.activity.OrderActivity;
import com.foodie.app.model.Product;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

public class RecommendedAdapter extends RecyclerView.Adapter<RecommendedAdapter.RecyclerViewHolder>{
    private Context mContext;
    private List<Product> listProduct;

    public RecommendedAdapter(Context mContext, List<Product> product) {
        this.mContext = mContext;
        this.listProduct = product;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.activity_main_recommended_item, parent, false);
        return new RecommendedAdapter.RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
        Product product = listProduct.get(position);
        Glide.with(mContext).load(product.getImage().getFileUrl()).placeholder(R.drawable.progress_animation).into(holder.image);
        holder.title.setText(product.getTitle());
        holder.price.setText(product.getPriceText());
        holder.image.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent productIntent = new Intent(mContext, OrderActivity.class);
                productIntent.putExtra("title", "Recommended");
                productIntent.putParcelableArrayListExtra("products", (ArrayList<Product>)listProduct);
                mContext.startActivity(productIntent);
            }
        });
        if (product.getOnsale() > 0) {
            holder.price.setPaintFlags(holder.price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.price.setTextColor(ContextCompat.getColor(mContext, R.color.colorGreyText));
            holder.priceSale.setVisibility(View.VISIBLE);
            holder.priceSale.setText(product.getOnsaleText());
        }
    }

    @Override
    public int getItemCount() {
        return listProduct.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView title;
        TextView price;
        TextView priceSale;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            title = itemView.findViewById(R.id.title);
            price = itemView.findViewById(R.id.price);
            priceSale = itemView.findViewById(R.id.priceSale);
        }
    }
}
