package com.foodie.app.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.foodie.app.R;
import com.foodie.app.fragments.ItemCheckout;
import com.foodie.app.model.Content;
import com.foodie.app.model.OrderHistoryModel;
import com.foodie.app.model.Product;

import java.util.List;

public class OrderHistoryAdapter extends RecyclerView.Adapter<OrderHistoryAdapter.RecycleViewHolder> {
    private Context mContext;
    private List<OrderHistoryModel> orderHistories;

    public OrderHistoryAdapter(Context mContext, List<OrderHistoryModel> orderHistories){
        this.mContext = mContext;
        this.orderHistories = orderHistories;
    }

    @NonNull
    @Override
    public RecycleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.order_history, parent, false);
        return new RecycleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecycleViewHolder holder, final int position) {
        final OrderHistoryModel orderHistory = this.orderHistories.get(position);
        if(this.orderHistories !=  null || this.orderHistories.size() > 0){
            holder.viewInformation.setVisibility(orderHistory.getVisiable() ? View.VISIBLE : View.GONE);
            holder.dateOrderd.setText(orderHistory.getDate());

            //Personal information
            holder.firstName.setText(orderHistory.getFirstName());
            holder.firstName.setKeyListener(null);
            holder.lastName.setText(orderHistory.getLastName());
            holder.lastName.setKeyListener(null);
            holder.phoneNumber.setText(orderHistory.getPhoneNumber());
            holder.phoneNumber.setKeyListener(null);
            holder.email.setText(orderHistory.getEmailAddress());
            holder.email.setKeyListener(null);
            holder.address.setText(orderHistory.getDeliveryAddress());
            holder.address.setKeyListener(null);
            holder.totalPrice.setText("$" + orderHistory.getTotalPrice());
            for(Product product : orderHistory.getItem()){
                holder.itemLayout.addView(new ItemCheckout(mContext, null, product));
            }
            //On click item
            holder.parentCard.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    if(orderHistory.getVisiable()){
                        holder.viewInformation.setVisibility(View.GONE);
                        orderHistory.setVisiable(false);
                    } else {
                        for(OrderHistoryModel item : orderHistories){
                            item.setVisiable(false);
                            item.getItem().clear();
                        }
                        orderHistory.setVisiable(true);
                        holder.viewInformation.setVisibility(View.VISIBLE);
                        notifyDataSetChanged();
                    }
                }
            });
        } else {
            holder.no_orders.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        if(orderHistories != null){
            return orderHistories.size();
        }
        return 0;
    }

    public class RecycleViewHolder extends RecyclerView.ViewHolder {
        TextView dateOrderd;
        Button addNew;
        TextView totalPrice;
        CardView parentCard;
        LinearLayout viewInformation;
        EditText firstName;
        EditText lastName;
        EditText phoneNumber;
        EditText email;
        EditText address;
        LinearLayout itemLayout;
        TextView no_orders;

        public RecycleViewHolder(View view){
            super(view);
            dateOrderd = view.findViewById(R.id.dateOrderd);
            addNew = view.findViewById(R.id.AddAsNew);
            totalPrice = view.findViewById(R.id.totalPrice);
            parentCard = view.findViewById(R.id.parent_cardview);
            viewInformation = view.findViewById(R.id.view_information);
            firstName = view.findViewById(R.id.firstName);
            lastName = view.findViewById(R.id.lastName);
            phoneNumber = view.findViewById(R.id.phone_number);
            email = view.findViewById(R.id.email);
            address = view.findViewById(R.id.address);
            itemLayout = view.findViewById(R.id.item);
            no_orders = view.findViewById(R.id.no_orders);
        }
    }
}
