package com.foodie.app.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.foodie.app.R;
import com.foodie.app.model.Product;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

public class YourCartAdapter extends RecyclerView.Adapter<YourCartAdapter.RecyclerViewHolder>{
    private Context mContext;
    private List<Product> listProduct;

    public YourCartAdapter(Context mContext, List<Product> listProduct) {
        this.mContext = mContext;
        this.listProduct = listProduct;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.your_cart_item_row, parent, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, final int position) {
        Product product = listProduct.get(position);
        Glide.with(mContext).load(product.getImage().getFileUrl()).apply(RequestOptions.circleCropTransform()).placeholder(R.drawable.progress_animation).into(holder.item_image);
        holder.item_title.setText(product.getTitle());
        holder.item_price.setText(product.getPriceText());
        if (product.getOnsale() > 0) {
            holder.item_price.setPaintFlags(holder.item_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.item_price.setTextColor(ContextCompat.getColor(mContext, R.color.colorGreyText));
            holder.item_price_sale.setVisibility(View.VISIBLE);
            holder.item_price_sale.setText(product.getOnsaleText());
        }

        holder.item_st.setNumber(String.valueOf(product.getQuantity()));
        holder.item_st.setOnValueChangeListener(new ElegantNumberButton.OnValueChangeListener() {
            @Override
            public void onValueChange(ElegantNumberButton view, int oldValue, int newValue) {
                boolean positive = (newValue - oldValue > 0 ? true : false);
                onStepperTouchCallbackListener.onStepperTouchCallback(position, newValue, positive);
            }
        });
        holder.item_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRemoveClickListener.onRemoveClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listProduct.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        ImageView item_image;
        TextView item_title;
        TextView item_price;
        TextView item_price_sale;
        ElegantNumberButton item_st;
        ImageView item_delete;
        public RecyclerViewHolder(View itemView) {
            super(itemView);
            item_image = itemView.findViewById(R.id.item_image);
            item_title = itemView.findViewById(R.id.item_title);
            item_price = itemView.findViewById(R.id.item_price);
            item_price_sale = itemView.findViewById(R.id.item_price_sale);
            item_st = itemView.findViewById(R.id.item_st);
            item_delete = itemView.findViewById(R.id.item_delete);
        }
    }

    public void setOnStepperTouchCallbackListener(OnStepperTouchCallbackListener onStepperTouchCallbackListener){
        this.onStepperTouchCallbackListener = onStepperTouchCallbackListener;
    }

    OnStepperTouchCallbackListener onStepperTouchCallbackListener;

    public interface OnStepperTouchCallbackListener{
        void onStepperTouchCallback(int position, int value, boolean positive);
    }

    public void setOnRemoveClickListener(OnRemoveClickListener onRemoveClickListener){
        this.onRemoveClickListener = onRemoveClickListener;
    }

    OnRemoveClickListener onRemoveClickListener;

    public interface OnRemoveClickListener{
        void onRemoveClick(int position);
    }

}
