package com.foodie.app.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.foodie.app.R;
import com.foodie.app.model.Slide;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

public class SlideAdapter extends PagerAdapter {
    private Context mContext;
    private List<Slide> listSlide;
    private LayoutInflater layoutInflater;

    public SlideAdapter(Context mContext, List<Slide> slide) {
        this.mContext = mContext;
        this.listSlide = slide;
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return listSlide.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return (view == (LinearLayout) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View item_view = layoutInflater.inflate(R.layout.activity_main_view_pager_item, container, false);
        ImageView imageView = item_view.findViewById(R.id.slide_image);
        Slide slide = listSlide.get(position);
        Glide.with(mContext).load(slide.getFileUrl()).placeholder(R.drawable.progress_animation).into(imageView);
        container.addView(item_view);
        return item_view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((LinearLayout) object);
    }
}
