package com.foodie.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.foodie.app.R;
import com.foodie.app.activity.OrderActivity;
import com.foodie.app.model.Content;
import com.foodie.app.model.Product;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.RecyclerViewHolder>{
    private Context mContext;
    private List<Content> listContent;

    public ProductAdapter(Context mContext, List<Content> listProduct) {
        this.mContext = mContext;
        this.listContent = listProduct;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.activity_main_product_item, parent, false);
        return new ProductAdapter.RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
        final Content content = listContent.get(position);
        Glide.with(mContext).load(mContext.getResources().getIdentifier(content.getImage(), "drawable", mContext.getPackageName())).placeholder(R.drawable.progress_animation).into(holder.image);
        holder.title.setText(content.getTitle());
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent productIntent = new Intent(mContext, OrderActivity.class);
                productIntent.putExtra("title", content.getTitle());
                productIntent.putParcelableArrayListExtra("products", (ArrayList<Product>) content.getProducts());
                mContext.startActivity(productIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listContent.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout mainLayout;
        TextView title;
        ImageView image;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            mainLayout = itemView.findViewById(R.id.mainLayout);
            title = itemView.findViewById(R.id.title);
            image = itemView.findViewById(R.id.image);
        }
    }
}
