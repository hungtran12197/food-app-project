package com.foodie.app.adapter;

import android.animation.Animator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.foodie.app.R;
import com.foodie.app.model.Product;
import com.tbuonomo.creativeviewpager.adapter.CreativePagerAdapter;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.InputStream;
import java.util.List;

public class OrderAdapter implements CreativePagerAdapter {
    private Context context;
    private List<Product> listProduct;

    public OrderAdapter(Context context, List<Product> listProduct) {
        this.context = context;
        this.listProduct = listProduct;
    }

    @Override
    public int getCount() {
        return listProduct.size();
    }

    @NotNull
    @Override
    public View instantiateContentItem(@NotNull LayoutInflater layoutInflater, @NotNull ViewGroup viewGroup, final int i) {
        // Inflate page layout
        View view = layoutInflater.inflate(R.layout.order_details_item_header, viewGroup, false);
        Product product = listProduct.get(i);
        ImageView imageProfile = view.findViewById(R.id.itemOrderDetailsImageProfile);
        Glide.with(context).load(product.getImage().getFileUrl()).placeholder(R.drawable.progress_animation).into(imageProfile);
        return view;
    }

    @NotNull
    @Override
    public View instantiateHeaderItem(@NotNull LayoutInflater layoutInflater, @NotNull ViewGroup viewGroup, final int i) {
        // Inflate page layout
        View view = layoutInflater.inflate(R.layout.order_details_item_content, viewGroup, false);
        // Bind the views
        TextView title = view.findViewById(R.id.itemOrderDetailsTitle);
        TextView price = view.findViewById(R.id.itemOrderDetailsPrice);
        TextView priceSale = view.findViewById(R.id.itemOrderDetailsPriceSale);
        TextView description = view.findViewById(R.id.itemOrderDetailsDescription);
        TextView discount = view.findViewById(R.id.itemOrderDetailsDiscount);
        ImageView image = view.findViewById(R.id.itemOrderDetailsImage);
        ImageView cartButton = view.findViewById(R.id.itemOrderDetailAddToCart);
        final LottieAnimationView animationView = view.findViewById(R.id.animation_view);
        animationView.addAnimatorListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                animationView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                animationView.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        Product product = listProduct.get(i);
        Glide.with(context).load(product.getImage().getFileUrl()).placeholder(R.drawable.progress_animation).into(image);
        title.setText(product.getTitle());
        price.setText(product.getPriceText());
        if (product.getOnsale() > 0) {
            discount.setText(product.getDiscountText());
            discount.setVisibility(View.VISIBLE);
            price.setPaintFlags(price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            price.setTextColor(context.getResources().getColor(R.color.colorGreyText));
            priceSale.setVisibility(View.VISIBLE);
            priceSale.setText(product.getOnsaleText());
        }

        description.setText(product.getDescription());
        cartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    animationView.setAnimation("checked_done.json");
                    animationView.playAnimation();
                    onItemClickListener.onItemClick(i);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        return view;
    }

    @Override
    public boolean isUpdatingBackgroundColor() {
        return true;
    }

    @Nullable
    @Override
    public Bitmap requestBitmapAtPosition(int i) {
        Bitmap bm = null;
        try {
            Product product = listProduct.get(i);
            InputStream is = new java.net.URL(product.getImage().getFileUrl()).openStream();
            bm = BitmapFactory.decodeStream(is);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bm;
    }

    OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener{
        void onItemClick(int position);
    }
}
