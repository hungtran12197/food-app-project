package com.foodie.app.util;

public class CommonKey {
    public static final String PERSONAL_INFORMATION = "personal_information";
    public static final String CARD_INFORMATION = "card_information";
    public static final String TOTAL_COUNT = "total_count";
    public static final String CART_DATA = "cart_data";
    public static final String INFORMATION_KEY = "information_key";
    public static final String CART_HISTORY_KEY = "cart_history_key";
    public static final String CART_HISTORY = "cart_history";

}
