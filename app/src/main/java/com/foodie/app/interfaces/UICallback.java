package com.foodie.app.interfaces;

public interface UICallback {
    void updateUI();
}
